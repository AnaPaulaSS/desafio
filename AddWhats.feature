#language:pt

Funcionalidade: Validar como adicionar um  contato

Cenario: Adicionar contato

Dado que eu esteja dentro do aplicativo
E eu clico na aba contatos
E eu clico em adicionar contato
E eu preencho o campo nome com "Ana"
E eu preencho o telefone "9999-7778"
E eu clico no botão "Salvar"
Então o contato é adicionado
