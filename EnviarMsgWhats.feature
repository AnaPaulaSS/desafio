#language:pt

Funcionalidade: Validar o envio de mensagem

Cenario: Digitar a mensagem e enviar

Dado que eu esteja dentro do aplicativo
E eu clico na aba contatos
E eu clico em cima  do contato "Wilson"
E eu digito "OI"
E eu clico no botão para enviar
Então espero que a mensagem seja enviada com sucesso
